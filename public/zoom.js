const mediaQuery = window.matchMedia("screen and (min-width: 2540px)");


function zoom(e) {
  if (e.matches) {
      let width = screen.width;
      let lastwidth = screen.width;
      if(width % 10 && width > lastwidth){
	  lastwidth = width;
	  let currentzoom = document.html.style.zoom.slice(0, -1);
	  let zoomint = parseInt(currentzoom);
	  zoomint = zoomint + 0.5;
	  document.html.style.zoom = zoomint.toString() = "%";
      }
  }
}

mediaQuery.addListener(zoom) // Attach listener function on state changes
zoom(mediaQuery) // Call listener function at run time
